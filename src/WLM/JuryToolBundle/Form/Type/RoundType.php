<?php
namespace WLM\JuryToolBundle\Form\Type;
use WLM\JuryToolBundle\Entity\Round;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;

class RoundType extends AbstractType
{

    private $formFactory;

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $atleastWidget = $this->formFactory->create('number', null, array(
                'label' => 'Enter the minimum amount of jurors that must review each photo',
                'required' => false
        ));
        
        $builder->add('userMode', 'choice', array(
                'label' => "Select the type of jury for this round: ",
                'choices' => array(
                        Round::$ROUND_USER_MODE_OPEN => "Open",
                        Round::$ROUND_USER_MODE_INVITATIONAL => "Invitational"
                ),
                'multiple' => false,
                'expanded' => false
        ))
            ->add('scoringMode', 'choice', array(
                'label' => "Select the type of scoring for this round: ",
                'choices' => array(
                        Round::$ROUND_SCORING_MODE_BINARY => "Binary",
                        Round::$ROUND_SCORING_MODE_STARS => "Stars-based"
                ),
                'multiple' => false,
                'expanded' => false
        ))
            ->add('input', 'choice', array(
                'label' => "Select the input for this round",
                'choices' => array(
                        Round::$ROUND_INPUT_CATEGORY => "Commons category",
                        Round::$ROUND_INPUT_PLAINTEXT => "Plain text file"
                ),
                'multiple' => false,
                'expanded' => true
        ))
            ->add('scoringPolicy', new ComposedFieldType(), array(
                'label' => "What's the scoring policy of this round?",
                'choices' => array(
                        Round::$ROUND_SCORING_POLICY_ONE2ONE => "Every photo is scored by exactly one juror",
                        Round::$ROUND_SCORING_POLICY_ATLEAST => "Every photo is scored by at least some number of jurors"
                ),
                'subwidgets' => array(
                        Round::$ROUND_SCORING_POLICY_ONE2ONE => null,
                        Round::$ROUND_SCORING_POLICY_ATLEAST => $atleastWidget->createView()
                )
        ))
            ->add('canViewOtherVotes', 'checkbox', array(
                'label' => "Can a user view other users' votes?"
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'WLM\JuryToolBundle\Entity\Round'
        ));
    }

    public function getName()
    {
        return 'wlm_jurytoolbundle_roundtype';
    }
}

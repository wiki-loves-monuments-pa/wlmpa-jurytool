<?php
namespace WLM\JuryToolBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class ComposedFieldType extends AbstractType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'multiple' => false,
                'expanded' => false,
                'subwidgets' => array()
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        
        $view->vars['subwidgets'] = $options['subwidgets'];
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'composed';
    }
}
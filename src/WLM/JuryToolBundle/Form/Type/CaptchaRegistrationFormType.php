<?php
namespace WLM\JuryToolBundle\Form\Type;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType;

class CaptchaRegistrationFormType extends RegistrationFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder->add('captcha', 'genemu_captcha', array(
                'property_path' => false
        ));
    }

    public function getName()
    {
    	return 'wlm_captcha_registration';
    }
}
<?php
namespace WLM\JuryToolBundle\Lib;
use WLM\JuryToolBundle\Entity\RoundImage;
use WLM\JuryToolBundle\Entity\Round;
use Monolog\Logger;
use Doctrine\Bundle\DoctrineBundle\Registry;
require_once ('botclasses.php');

class CategoryLoader
{

    private $logger;

    private $doctrine;

    public function __construct(Logger $logger, Registry $doctrine)
    {
        $this->logger = $logger;
        $this->doctrine = $doctrine;
    }

    public function addImagesFromCategory(Round $round, $categoryName, $checkPrevious = false)
    {
        $this->logger->info("Loading category $categoryName");
        
        $commons = new \wikipedia('http://commons.wikipedia.org/w/api.php');
        $timestamp = new \DateTime();
        $counter = 0;
        $gcmcontinue = null;
        
        $commons->quiet = true;
        
        do {
            $queryResults = $commons->query("?action=query&generator=categorymembers&gcmtitle=Category:" . urlencode($categoryName) . "&prop=imageinfo&iiprop=url|size&format=php&iiurlwidth=800" . ($gcmcontinue ? "&gcmcontinue=$gcmcontinue" : ""));
            
            if (! array_key_exists('query', $queryResults)) {
                return null;
            }
            
            if (array_key_exists('query-continue', $queryResults)) {
                $gcmcontinue = $queryResults['query-continue']['categorymembers']['gcmcontinue'];
            } else {
                $gcmcontinue = null;
            }
            
            $this->processQueryResults($round, $queryResults, $counter, $timestamp, $checkPrevious, $gcmcontinue == null);
        } while ($gcmcontinue);
        
        $this->doctrine->getManager()->flush();
        $this->logger->info("Finished loading category $categoryName");
        
        return $counter;
    }

    public function addImagesFromList(Round $round, array $images)
    {
        $this->logger->info("Loading " . count($images) . " images");
        $timestamp = new \DateTime();
        $counter = 0;
        $commons = new \wikipedia('http://commons.wikipedia.org/w/api.php');
        $commons->quiet = true;
        
        while (count($images) > 0) {
            $imageBatch = array_splice($images, 0, 50);
            $queryResults = $commons->query("?action=query&prop=imageinfo&iiprop=url|size&format=php&iiurlwidth=800&titles=File:" . urlencode(join("|File:", $imageBatch)));
            
            $this->processQueryResults($round, $queryResults, $counter, $timestamp, true, count($images) == 0);
        }
        
        $this->doctrine->getManager()->flush();
        $this->logger->info("Finished loading " . count($images) . " images");
        
        return $counter;
    }

    private function processQueryResults(Round $round, &$queryResults, &$counter,\DateTime $now, $checkPrevious, $last)
    {
        $batchSize = 1000;
        $em = $this->doctrine->getManager();
        $roundImageRepo = $this->doctrine->getRepository('WLMJuryToolBundle:RoundImage');
        
        foreach ($queryResults['query']['pages'] as $p) {
            if (! array_key_exists('imageinfo', $p)) {
                continue;
            }
            
            $img = new RoundImage();
            $thumburl = $p['imageinfo'][0]['thumburl'];
            $imgWidth = floatval($p['imageinfo'][0]['width']);
            $imgHeight = floatval($p['imageinfo'][0]['height']);
            
            $img->setCommonsId($p['pageid']);
            $img->setName($p['title']);
            $img->setUrl($p['imageinfo'][0]['url']);
            $img->setIsFinalized(false);
            
            if ($imgHeight > 0) {
                if (($imgWidth / $imgHeight) < (800.0 / 600.0)) {
                    $newWidth = intval(600 * $imgWidth / $imgHeight);
                    $thumburl = preg_replace("/\/800px/", "/{$newWidth}px", $thumburl, 1);
                }
                
                $img->setThumbnail($thumburl);
                
                if (! $checkPrevious) {
                    $img->setRound($round);
                    $img->setTimestamp($now);
                    $em->persist($img);
                } else {
                    $previousImage = $roundImageRepo->findOneBy(array(
                            'commonsId' => $img->getCommonsId(),
                            'round' => $round
                    ));
                    
                    if ($previousImage == null) {
                        $img->setRound($round);
                        $img->setTimestamp($now);
                        $em->persist($img);
                    } else {
                        $previousImage->setTimestamp($now);
                        
                        if ($previousImage->getName() != $img->getName()) {
                            // File moved
                            $previousImage->setName($img->getName());
                            // $movedImages ++;
                        }
                        
                        $em->persist($previousImage);
                    }
                }
                
                $counter ++;
                
                if (($counter % $batchSize) == 0) {
                    $em->flush();
                    $em->clear('WLMJuryToolBundle:RoundImage');
                }
            } else {
                $this->logger->warning("Image {$p['title']} has no height?");
            }
        }
    }
}
<?php
namespace WLM\JuryToolBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WLM\JuryToolBundle\Controller\DefaultController;
use WLM\JuryToolBundle\WLMJuryToolBundle;

class UpdateRoundCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('wlm:update-round')->setDescription("Update the current round with new photos from the associated category");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrineService = $this->getContainer()->get('doctrine');
        $em = $doctrineService->getManager();
        $roundRepo = $doctrineService->getRepository('WLMJuryToolBundle:Round');
        $roundImageRepo = $doctrineService->getRepository('WLMJuryToolBundle:RoundImage');
        $currentRound = $roundRepo->findOneBy(array(
                'isCurrent' => true
        ));
        
        $matches = array();
        $addedImages = 0;
        // $movedImages = 0;
        
        if ($currentRound == null) {
            $output->writeln("No current round");
        }
        
        if (preg_match("/^category\:(.*)/", $currentRound->getInput(), $matches) === 0) {
            $output->writeln("Current round input is not a category");
            
            return;
        }
        
        $output->writeln("Current round input category is ${matches[1]}");
        
        $allImages = $images = $this->getContainer()
            ->get('category_loader')
            ->addImagesFromCategory($currentRound, $matches[1], true);
        
        $em->flush();
        
        // Cleanup old images
        $now = $em->createQuery('SELECT MAX(i.timestamp) FROM WLMJuryToolBundle:RoundImage i')->getSingleScalarResult();
        
        $em->createQuery("DELETE FROM {$currentRound->getScoreClassName()} s WHERE s.roundImage IN (SELECT i FROM WLMJuryToolBundle:RoundImage i WHERE i.round = :round AND i.timestamp < :now)")
            ->setParameter('round', $currentRound)
            ->setParameter('now', $now)
            ->execute();
        
        $em->createQuery("DELETE FROM WLMJuryToolBundle:RoundImageLock l WHERE l.roundImage IN (SELECT i FROM WLMJuryToolBundle:RoundImage i WHERE i.round = :round AND i.timestamp < :now)")
            ->setParameter('round', $currentRound)
            ->setParameter('now', $now)
            ->execute();
        
        $deletedImages = $em->createQuery('DELETE FROM WLMJuryToolBundle:RoundImage i WHERE i.round = :round AND i.timestamp < :now')
            ->setParameter('round', $currentRound)
            ->setParameter('now', $now)
            ->execute();
        
        $output->writeln("Total of $allImages images in current round, $deletedImages deleted");
    }
}

?>
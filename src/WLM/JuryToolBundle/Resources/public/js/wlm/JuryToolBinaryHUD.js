define([ "dojo/_base/declare", "js/wlm/JuryToolHUD", "dojo/text!js/wlm/JuryToolBinaryHUD.html", "dojo/on", "dojo/_base/lang", "dojo/dom-style" ], function(declare, JuryToolHUD, template, on, lang, domStyle) {
	return declare([ JuryToolHUD ], {
		templateString : template,

		postCreate : function() {
			this.inherited(arguments);
			this.own(on(this.yesButton, "click", lang.hitch(this, "scoreClicked", 1)));
			this.own(on(this.noButton, "click", lang.hitch(this, "scoreClicked", 0)));
		},

		imageChanged : function(imageInfo) {
			// Reset all counters
			this.yesCounter.innerHTML = "";
			this.noCounter.innerHTML = "";

			// Reset all borders
			domStyle.set(this.yesButton, "border", "none");
			domStyle.set(this.noButton, "border", "none");

			this.inherited(arguments);
		},

		processScore : function(score) {
			if (score != null) {
				// TODO probably relocate the node to align it to the other
				// nodes
				domStyle.set(this[(score == 1) ? 'yesButton' : 'noButton'], "border", "2px solid red");
			}
		},

		processOtherScores : function(scores) {
			for ( var s in scores) {
				this[(s == 1) ? 'yesCounter' : 'noCounter'].innerHTML = scores[s];
			}
		}
	});
});
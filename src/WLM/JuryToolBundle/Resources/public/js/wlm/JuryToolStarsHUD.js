define([ "dojo/_base/declare", "js/wlm/JuryToolHUD", "dojo/text!js/wlm/JuryToolStarsHUD.html", "dojo/on", "dojo/_base/lang", "dojo/dom-style" ], function(declare, JuryToolHUD, template, on, lang, domStyle) {
	return declare([ JuryToolHUD ], {
		templateString : template,

		postCreate : function() {
			this.inherited(arguments);
			this.own(on(this.stars0, "click", lang.hitch(this, "scoreClicked", 0)));
			this.own(on(this.stars1, "click", lang.hitch(this, "scoreClicked", 1)));
			this.own(on(this.stars2, "click", lang.hitch(this, "scoreClicked", 2)));
			this.own(on(this.stars3, "click", lang.hitch(this, "scoreClicked", 3)));
			this.own(on(this.stars4, "click", lang.hitch(this, "scoreClicked", 4)));
			this.own(on(this.stars5, "click", lang.hitch(this, "scoreClicked", 5)));
		},

		imageChanged : function(imageInfo) {
			// Reset all counters
			this.stars0Counter.innerHTML = "";
			this.stars1Counter.innerHTML = "";
			this.stars2Counter.innerHTML = "";
			this.stars3Counter.innerHTML = "";
			this.stars4Counter.innerHTML = "";
			this.stars5Counter.innerHTML = "";

			// Reset all borders
			domStyle.set(this.stars0, "border", "none");
			domStyle.set(this.stars1, "border", "none");
			domStyle.set(this.stars2, "border", "none");
			domStyle.set(this.stars3, "border", "none");
			domStyle.set(this.stars4, "border", "none");
			domStyle.set(this.stars5, "border", "none");

			this.inherited(arguments);
		},

		processScore : function(score) {
			if (score != null) {
				// TODO probably relocate the node to align it to the other
				// nodes
				domStyle.set(this['stars' + score], "border", "2px solid red");
			}
		},

		processOtherScores : function(scores) {
			for ( var s in scores) {
				this['stars' + s + 'Counter'].innerHTML = scores[s];
			}
		}
	});
});

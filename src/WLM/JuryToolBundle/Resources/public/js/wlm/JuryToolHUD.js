define([ "dojo/_base/declare", "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dojo/dom-geometry", "dojo/dom-style", "dojo/_base/fx", "dojo/_base/lang", "dojo/on", "dojo/mouse", "dijit/registry", "dojo/topic", "dojo/request/xhr", "dojo/dom" ], function(declare, _WidgetBase, _TemplatedMixin, geometry, domStyle, fx, lang, on, mouse, registry, topic, xhr, dom) {
	return declare([ _WidgetBase, _TemplatedMixin ], {
		shouldRequestOtherVotes : false,

		currentImageInfo : null,

		postCreate : function() {
			var overNode = registry.byId(this.over);
			var parentNode = null;

			if (overNode == undefined) {
				// This is a regular image node
				var overNode = dom.byId(this.over);
				var parentNode = this.domNode.parentNode;
				var imagePos = geometry.position(overNode);

				domStyle.set(this.domNode, {
					top : imagePos.y + 'px',
					left : imagePos.x + 'px',
					zIndex : '0'
				});

				this.requestVotes();
			} else {
				var parentNode = overNode.outerNode;
				var largeNodePos = geometry.position(overNode.largeNode);
				var navNodePos = geometry.position(overNode.navNode);

				overNode.domNode.appendChild(this.domNode);
				topic.subscribe(overNode.getShowTopicName(), lang.hitch(this, "imageChanged"));

				domStyle.set(this.domNode, {
					top : navNodePos.h + 'px',
					left : '0px',
					zIndex : '0'
				});
			}

			this.own(on(parentNode, mouse.enter, lang.hitch(this, "popEnter")));
			this.own(on(parentNode, mouse.leave, lang.hitch(this, "popLeave")));
		},

		popEnter : function() {
			fx.fadeIn({
				node : this.domNode
			}).play();
		},

		popLeave : function() {
			fx.fadeOut({
				node : this.domNode
			}).play();
		},

		exifMetadata : '',

		scoreClicked : function(vote) {
			var overNode = registry.byId(this.over);

			if (overNode != undefined) {
				// TODO Fix access to private member of Data Store
				var imageID = overNode.imageStore._arrayOfAllItems[this.currentImageInfo.index].id[0];

				xhr(Routing.generate('wlm_jury_tool_score', {
					imageID : imageID
				}), {
					handleAs : 'xml',
					method : 'POST',
					data : {
						score : vote
					}
				}).then(lang.hitch(overNode, "showNextImage", false));
			} else {
				xhr(Routing.generate('wlm_jury_tool_score', {
					imageID : this.currentImageID
				}), {
					handleAs : 'xml',
					method : 'POST',
					data : {
						score : vote
					}
				}).then(function() {
					window.location.reload(true)
				});
			}
		},

		imageChanged : function(imageInfo) {
			this.currentImageInfo = imageInfo;
			this.requestVotes();
		},

		requestVotes : function() {
			var imageID = null;
			var overNode = registry.byId(this.over);

			if (overNode != undefined) {
				// TODO Fix access to private member of Data Store
				imageID = overNode.imageStore._arrayOfAllItems[this.currentImageInfo.index].id[0];
			} else {
				imageID = this.currentImageID;
			}

			xhr(Routing.generate('wlm_jury_tool_score', {
				imageID : imageID
			}), {
				handleAs : 'xml'
			}).then(lang.hitch(this, "processScoreXML"));

			if (this.shouldRequestOtherVotes) {
				xhr(Routing.generate('wlm_jury_tool_scores', {
					imageID : imageID
				}), {
					handleAs : 'xml'
				}).then(lang.hitch(this, "processOtherScoresXML"));
			}
		},

		processScoreXML : function(scoreXML) {
			var scoreNodes = scoreXML.getElementsByTagName('score');
			var score = scoreNodes[0].getAttribute('score');
			
			this.processScore(score);
		},

		processOtherScoresXML : function(scoresXML) {
			var scoreNodes = scoresXML.getElementsByTagName('score');
			var otherScores = {};

			for ( var i = 0; i < scoreNodes.length; i++) {
				var userID = scoreNodes[i].getAttribute('user');
				var score = scoreNodes[i].getAttribute('score');

				if (score in otherScores) {
					otherScores[score]++;
				} else {
					otherScores[score] = 1;
				}
			}

			this.processOtherScores(otherScores);
		}
	});
});
<?php
namespace WLM\JuryToolBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WLM\JuryToolBundle\Entity;
use WLM\JuryToolBundle\Entity\RoundRepository;
use WLM\JuryToolBundle\Entity\Round;
use WLM\JuryToolBundle\Form\Type\RoundType;

class ConfigurationController extends Controller
{

    public function indexAction()
    {
        $request = $this->getRequest();
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $rounds = $roundRepo->findAll();
        $round = new Round();
        $roundForm = $this->createForm($this->get('round_type'), $round);
        
        if ($request->getMethod() == "GET") {
            return $this->render('WLMJuryToolBundle:Configuration:round.html.twig', array(
                    'round_form' => $roundForm->createView(),
                    'has_previous_round' => (count($rounds) > 0)
            ));
        }
        
        $roundForm->bind($request);
        
        if ($roundForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            // Calculate policy
            $policy = $round->getScoringPolicy();
            
            if ($policy == Round::$ROUND_SCORING_POLICY_ATLEAST) {
                $policy .= ":" . $request->request->getInt('number');
            }
            
            $round->setScoringPolicy($policy);
            $round->setIsCurrent(false);
            $em->persist($round);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('wlm_jury_tool_admin', array(
                'id' => $round->getId()
        )), 303);
    }
}
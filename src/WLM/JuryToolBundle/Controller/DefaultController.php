<?php
namespace WLM\JuryToolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\Query\Expr;
use Doctrine\DBAL\Connection;
use WLM\JuryToolBundle\Entity\RoundRepository;
use WLM\JuryToolBundle\Entity\Round;
use WLM\JuryToolBundle\Entity\RoundImage;
use WLM\JuryToolBundle\Entity\RoundImageLock;
use WLM\JuryToolBundle\Entity\ImageStarScore;
use WLM\JuryToolBundle\Entity\ImageBinaryScore;
use Symfony\Component\Validator\Constraints\Date;

class DefaultController extends Controller
{

    public function indexAction()
    {
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $currentRound = $roundRepo->findOneBy(array(
                'isCurrent' => true
        ));
        
        if ($currentRound == null) {
            if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
                $rounds = $roundRepo->findAll();
                
                if (count($rounds) > 0) {
                    return $this->redirect($this->generateUrl('wlm_jury_tool_admin', array(
                            'id' => $rounds[0]->getId()
                    )), 303);
                }
                
                return $this->redirect($this->generateUrl('wlm_jury_tool_config'), 303);
            }
            
            return $this->render('WLMJuryToolBundle:Default:notcurrentround.html.twig', array(
                    'upcoming' => true
            ));
        }
        
        return $this->redirect($this->generateUrl('wlm_jury_tool_round', array(
                'id' => $currentRound->getId()
        )));
    }

    public function roundAction($id)
    {
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $currentRound = $roundRepo->find($id);
        $user = $this->getUser();
        
        if (! $currentRound->getIsCurrent()) {
            // Tried accessing a round that is not the current one
            $realCurrentRound = $roundRepo->findOneBy(array(
                    'isCurrent' => true
            ));
            
            return $this->render('WLMJuryToolBundle:Default:notcurrentround.html.twig', array(
                    'upcoming' => ($realCurrentRound) ? $realCurrentRound->getId() < $currentRound->getId() : true
            ));
        }
        
        if ($currentRound->getUserMode() == Round::$ROUND_USER_MODE_INVITATIONAL) {
            if (! $currentRound->getUsers()->contains($user)) {
                return $this->render('WLMJuryToolBundle:Default:notinvited.html.twig');
            }
        }
        
        if ($currentRound->getScoringPolicy() == Round::$ROUND_SCORING_POLICY_ONE2ONE) {
            $em = $this->getDoctrine()->getManager();
            
            $em->beginTransaction();
            $em->getConnection()->setTransactionIsolation(Connection::TRANSACTION_SERIALIZABLE);
            
            // Cleanup old locks
            $em->createQuery('DELETE FROM WLMJuryToolBundle:RoundImageLock i WHERE i.timestamp < :fiveMinutesAgo')
                ->setParameter('fiveMinutesAgo', date_sub(new \DateTime(), new \DateInterval("PT5M")))
                ->execute();
            $em->flush(); // needed?
            
            $images = $em->createQueryBuilder()
                ->select('i')
                ->from('WLMJuryToolBundle:RoundImage', 'i')
                ->leftJoin($currentRound->getScoreClassName(), 's', Expr\Join::WITH, 'i = s.roundImage')
                ->leftJoin('WLMJuryToolBundle:RoundImageLock', 'l', Expr\Join::WITH, 'i = l.roundImage AND l.user <> :user')
                ->where('s.roundImage IS NULL')
                ->andWhere('l.roundImage IS NULL')
                ->andWhere('i.round = :round')
                ->setParameter(':round', $currentRound)
                ->setParameter(':user', $user)
                ->getQuery()
                ->setMaxResults(1)
                ->getResult();
            
            if (count($images) == 1) {
                if ($images[0]->getLock() == null) {
                    // Lock image for this user
                    $imageLock = new RoundImageLock();
                    
                    $imageLock->setRoundImage($images[0]);
                    $imageLock->setUser($user);
                    $imageLock->setTimestamp(new \DateTime());
                    
                    $em->persist($imageLock);
                } else {
                    // Update existing lock timestamp
                    $images[0]->getLock()->setTimeStamp(new \DateTime());
                }
            }
            
            $em->commit();
            $em->flush();
            
            return $this->render('WLMJuryToolBundle:Default:singleimage.html.twig', array(
                    'image' => count($images) == 1 ? $images[0] : null,
                    'round' => $currentRound
            ));
        }
        
        return $this->render('WLMJuryToolBundle:Default:index.html.twig', array(
                'round' => $currentRound
        ));
    }

    public function imageListAction($id)
    {
        $user = $this->getUser();
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $currentRound = $roundRepo->find($id);
        
        $query = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('i.id, i.name, i.url, i.thumbnail, COUNT(s.roundImage) AS voteCount')
            ->from('WLMJuryToolBundle:RoundImage', 'i')
            ->leftJoin($currentRound->getScoreClassName(), 's', Expr\Join::WITH, 'i = s.roundImage')
            ->leftJoin($currentRound->getScoreClassName(), 's2', Expr\Join::WITH, 'i = s2.roundImage AND s2.user = :user')
            ->where('s2.user IS NULL')
            ->andWhere('i.round = :round')
            ->orderBy('voteCount', 'ASC')
            ->groupBy('i.id, i.name, i.url, i.thumbnail')
            ->setParameter(':round', $currentRound)
            ->setParameter(':user', $user)
            ->getQuery();
        
        return $this->render('WLMJuryToolBundle:Default:imagelist.json.twig', array(
                'results' => $query->getArrayResult()
        ));
    }

    public function scoreAction($imageID, $all = false)
    {
        $scores = array();
        $imageRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:RoundImage');
        $votedImage = $imageRepo->find($imageID);
        $currentRound = $votedImage->getRound();
        $user = $this->getUser();
        $scoresRepo = $this->getDoctrine()->getRepository($currentRound->getScoreClassName());
        $score = $scoresRepo->findOneBy(array(
                'roundImage' => $votedImage,
                'user' => $user
        ));
        
        if ($this->getRequest()->getMethod() === "POST") {
            $request = $this->getRequest()->request;
            $em = $this->getDoctrine()->getManager();
            
            switch ($currentRound->getScoringMode()) {
                case "stars":
                    if ($score == null) {
                        $score = new ImageStarScore();
                    }
                    
                    $score->setScore($request->getInt('score'));
                    
                    break;
                case "binary":
                    if ($score == null) {
                        $score = new ImageBinaryScore();
                    }
                    
                    $score->setScore($request->getInt('score') === 1);
                    
                    break;
                default:
                    break;
            }
            
            if ($score !== null) {
                $scores[] = $score;
                $score->setUser($user);
                $score->setRoundImage($votedImage);
                $em->persist($score);
            }
            
            $em->flush();
        } else {
            if ($all === true) {
                $scores = $votedImage->getScores();
            } else {
                // We already queried for current users score
                if ($score != null) {
                    $scores[] = $score;
                }
            }
        }
        
        return $this->render('WLMJuryToolBundle:Default:score.xml.twig', array(
                'status' => "OK",
                'mode' => $currentRound->getScoringMode(),
                'scores' => $scores
        ));
    }
}

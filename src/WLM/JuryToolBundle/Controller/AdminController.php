<?php
namespace WLM\JuryToolBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\Query\Expr;
use WLM\JuryToolBundle\Entity\Round;

class AdminController extends Controller
{

    public function indexAction($id)
    {
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $round = $roundRepo->find($id);
        $templateName = 'WLMJuryToolBundle:Admin:round.html.twig';
        $templateData = array(
                'round' => $round,
                'users' => null,
                'stats' => array()
        );
        
        // Query for the total number of photos in this round
        $templateData['stats']['numberOfPhotos'] = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('COUNT(i)')
            ->from('WLMJuryToolBundle:RoundImage', 'i')
            ->where('i.round = :round')
            ->setParameter(':round', $round)
            ->getQuery()
            ->getSingleScalarResult();
        
        $templateData['canRoundBeClosed'] = ($templateData['stats']['numberOfPhotos'] > 0) && $this->canBeClosed($round);
        
        if ($round->getUserMode() == Round::$ROUND_USER_MODE_INVITATIONAL) {
            $userRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:User');
            $scoringRepo = $this->getDoctrine()->getRepository($round->getScoreClassName());
            $templateData['users'] = array_diff($userRepo->findAll(), $round->getUsers()->toArray());
            
            foreach ($round->getUsers() as $user) {
                $userStats = array();
                
                $userScores = $this->getDoctrine()
                    ->getManager()
                    ->createQueryBuilder()
                    ->select('s.score, COUNT(s.score) AS scoreCount')
                    ->from($round->getScoreClassName(), 's')
                    ->innerJoin('WLMJuryToolBundle:RoundImage', 'ri', Expr\Join::WITH, 's.roundImage = ri AND ri.round = :round')
                    ->where('s.user = :user')
                    ->groupBy('s.score')
                    ->setParameter(':round', $round)
                    ->setParameter(':user', $user)
                    ->getQuery()
                    ->execute();
                
                if ($round->getScoringMode() == Round::$ROUND_SCORING_MODE_STARS) {
                    $userStats['tally'] = array_pad(array(), 6, 0);
                } else if ($round->getScoringMode() == Round::$ROUND_SCORING_MODE_BINARY) {
                    $userStats['tally'] = array_pad(array(), 2, 0);
                }
                
                foreach ($userScores as $score) {
                    $userStats['tally'][$score['score']] = $score['scoreCount'];
                }
                
                $userStats['total'] = array_sum(array_values($userStats['tally']));
                
                $templateData['stats'][$user->getUsername()] = $userStats;
            }
        }
        
        $scoringTally = array();
        $scores = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('s.score, COUNT(s.score) AS scoreCount')
            ->from($round->getScoreClassName(), 's')
            ->innerJoin('WLMJuryToolBundle:RoundImage', 'ri', Expr\Join::WITH, 's.roundImage = ri AND ri.round = :round')
            ->groupBy('s.score')
            ->setParameter(':round', $round)
            ->getQuery()
            ->execute();
        
        if ($round->getScoringMode() == Round::$ROUND_SCORING_MODE_STARS) {
            $scoringTally = array_pad(array(), 6, 0);
        } else if ($round->getScoringMode() == Round::$ROUND_SCORING_MODE_BINARY) {
            $scoringTally = array_pad(array(), 2, 0);
        }
        
        foreach ($scores as $score) {
            $scoringTally[$score['score']] = $score['scoreCount'];
        }
        
        $templateData['stats']['total'] = array_sum(array_values($scoringTally));
        $templateData['stats']['tally'] = $scoringTally;
        
        if ($templateData['stats']['numberOfPhotos'] == 0) {
            switch ($round->getInput()) {
                case "category":
                    $templateName = 'WLMJuryToolBundle:Admin:roundfromcategory.html.twig';
                    
                    break;
                case "plaintext":
                    $templateName = 'WLMJuryToolBundle:Admin:roundfromplaintext.html.twig';
                    
                    break;
                case "previous":
                    $templateName = 'WLMJuryToolBundle:Admin:roundfromprevious.html.twig';
                    
                    break;
                default:
                    break;
            }
        }
        
        return $this->render($templateName, $templateData);
    }

    public function loadAction($id)
    {
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $currentRound = $roundRepo->find($id);
        $images = 0;
        
        if (count($currentRound->getImages()) == 0) {
            $em = $this->getDoctrine()->getManager();
            
            switch ($currentRound->getInput()) {
                case Round::$ROUND_INPUT_CATEGORY:
                    $request = $this->getRequest()->request;
                    $commonscat = $request->get('commonscat');
                    
                    $images = $this->get('category_loader')->addImagesFromCategory($currentRound, $commonscat);
                    
                    if ($images != null) {
                        if ($request->has('recurrent')) {
                            $commandPrefix = $this->container->hasParameter('wlm.country_code') ? "SYMFONY__WLM__COUNTRY_CODE={$this->container->getParameter('wlm.country_code')} " : "";
                            $command = $commandPrefix . $this->get('kernel')->getRootDir() . "/console wlm:update-round";
                            $currentRound->setInput("category:$commonscat");
                            $crontabHandle = popen("crontab -l", "r");
                            $crontab = "";
                            
                            while (! feof($crontabHandle)) {
                                $nextCrontabLine = fgets($crontabHandle);
                                
                                if (strrpos($nextCrontabLine, $command) === false) {
                                    $crontab .= $nextCrontabLine;
                                }
                            }
                            
                            pclose($crontabHandle);
                            
                            $crontab .= date("i H") . " * * * $command\n";
                            
                            $crontabHandle = popen("crontab -", "w");
                            fwrite($crontabHandle, $crontab);
                            pclose($crontabHandle);
                        }
                    } else {
                        $errorMsg = $this->get('translator')->trans("Category name is incorrect or the category is empty");
                        
                        $this->get('session')->setFlash('error', $errorMsg);
                    }
                    
                    break;
                case Round::$ROUND_INPUT_PLAINTEXT:
                    $file = $this->getRequest()->files->get('plaintext');
                    $image_list = file($file->getPathName(), FILE_IGNORE_NEW_LINES);
                    $images = $this->get('category_loader')->addImagesFromList($currentRound, $image_list);
                    
                    break;
                default:
                    
                    break;
            }
            
            if ($images > 0) {
                // Should only be one, but one never knows...
                $allCurrentRounds = $roundRepo->findBy(array(
                        'isCurrent' => true
                ));
                
                foreach ($allCurrentRounds as $r) {
                    $r->setIsCurrent(false);
                }
                
                $currentRound->setIsCurrent(true);
                
                $em->flush();
            }
        }
        
        return $this->redirect($this->generateUrl('wlm_jury_tool_admin', array(
                'id' => $currentRound->getId()
        )));
    }

    public function aclAction($id)
    {
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $userRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:User');
        $round = $roundRepo->find($id);
        
        $userids = $this->getRequest()->request->get('userids');
        
        foreach ($userids as $userid) {
            $user = $userRepo->find($userid);
            $user->addRound($round);
        }
        
        $this->getDoctrine()
            ->getManager()
            ->flush();
        
        return $this->redirect($this->generateUrl('wlm_jury_tool_admin', array(
                'id' => $round->getId()
        )));
    }

    public function exportAction($id, $username = null, $score = null)
    {
        $roundRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:Round');
        $round = $roundRepo->find($id);
        $imageList = array();
        
        if ($username != null) {
            $userRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:User');
            $user = $userRepo->findOneBy(array(
                    'username' => $username
            ));
            
            if ($score != null) {
                $operator = "=";
                
                $this->parseScoreString($score, $operator);
                
                $imageList = $this->getDoctrine()
                    ->getManager()
                    ->createQueryBuilder()
                    ->select('ri')
                    ->from($round->getScoreClassName(), 's')
                    ->innerJoin('WLMJuryToolBundle:RoundImage', 'ri', Expr\Join::WITH, 's.roundImage = ri AND ri.round = :round')
                    ->where('s.user = :user')
                    ->andWhere("s.score $operator $score")
                    ->setParameter(':round', $round)
                    ->setParameter(':user', $user)
                    ->getQuery()
                    ->execute();
            } else {
                $imageList = $this->getDoctrine()
                    ->getManager()
                    ->createQueryBuilder()
                    ->select('ri')
                    ->from($round->getScoreClassName(), 's')
                    ->innerJoin('WLMJuryToolBundle:RoundImage', 'ri', Expr\Join::WITH, 's.roundImage = ri AND ri.round = :round')
                    ->where('s.user = :user')
                    ->setParameter(':round', $round)
                    ->setParameter(':user', $user)
                    ->getQuery()
                    ->execute();
            }
        } else {
            if ($score != null) {
                $operator = "=";
                
                $this->parseScoreString($score, $operator);
                
                $imageList = $this->getDoctrine()
                    ->getManager()
                    ->createQueryBuilder()
                    ->select('DISTINCT ri')
                    ->from($round->getScoreClassName(), 's')
                    ->innerJoin('WLMJuryToolBundle:RoundImage', 'ri', Expr\Join::WITH, 's.roundImage = ri AND ri.round = :round')
                    ->where("s.score $operator $score")
                    ->setParameter(':round', $round)
                    ->getQuery()
                    ->execute();
            } else {
                $roundImageRepo = $this->getDoctrine()->getRepository('WLMJuryToolBundle:RoundImage');
                $imageList = $roundImageRepo->findBy(array(
                        'round' => $round
                ));
            }
        }
        
        return $this->render("WLMJuryToolBundle:Admin:imagelist.txt.twig", array(
                'imageList' => $imageList
        ));
    }

    private function parseScoreString(&$score, &$operator)
    {
        if (preg_match("/(\\d)([\\+\\-]?)/", $score, $match)) {
            $score = (int) $match[1];
            
            if ($match[2] == "+") {
                $operator = ">=";
            } else if ($match[2] == "-") {
                $operator = "<=";
            }
        }
    }

    private function canBeClosed(Round $round)
    {
        if ($round->getScoringPolicy() == Round::$ROUND_SCORING_POLICY_ONE2ONE) {
            $numberOfPhotosWithoutScore = $this->getDoctrine()
                ->getManager()
                ->createQueryBuilder()
                ->select('COUNT(i)')
                ->from('WLMJuryToolBundle:RoundImage', 'i')
                ->leftJoin($round->getScoreClassName(), 's', Expr\Join::WITH, 's.roundImage = i')
                ->where('i.round = :round')
                ->andWhere('s IS NULL')
                ->setParameter(':round', $round)
                ->getQuery()
                ->getSingleScalarResult();
            
            return $numberOfPhotosWithoutScore == 0;
        } else if (substr($round->getScoringPolicy(), 0, strlen(Round::$ROUND_SCORING_POLICY_ATLEAST)) == Round::$ROUND_SCORING_POLICY_ATLEAST) {
            $splitPolicy = split(":", $round->getScoringPolicy());
            $numberOfScores = $this->getDoctrine()
                ->getManager()
                ->createQueryBuilder()
                ->select('COUNT(s)')
                ->from($round->getScoreClassName(), 's')
                ->innerJoin('WLMJuryToolBundle:RoundImage', 'ri', Expr\Join::WITH, 's.roundImage = ri AND ri.round = :round')
                ->setParameter(':round', $round)
                ->getQuery()
                ->getSingleScalarResult();
            $numberOfPhotosUnderreviewed = $this->getDoctrine()
                ->getManager()
                ->createQueryBuilder()
                ->select('COUNT(ri)')
                ->from('WLMJuryToolBundle:RoundImage', 'ri')
                ->innerJoin($round->getScoreClassName(), 's', Expr\Join::WITH, 's.roundImage = ri')
                ->where('ri.round = :round')
                ->groupBy('ri')
                ->having('COUNT(s) < :minimum')
                ->setParameter(':round', $round)
                ->setParameter(':minimum', (int) $splitPolicy[1])
                ->getQuery()
                ->execute();
            
            return ($numberOfScores > 0) && (count($numberOfPhotosUnderreviewed) == 0);
        }
        
        return false;
    }
}
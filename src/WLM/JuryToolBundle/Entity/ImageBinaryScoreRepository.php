<?php

namespace WLM\JuryToolBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ImageBinaryScoreRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ImageBinaryScoreRepository extends EntityRepository
{
}

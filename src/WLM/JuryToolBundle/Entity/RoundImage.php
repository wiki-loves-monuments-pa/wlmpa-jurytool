<?php

namespace WLM\JuryToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundImage
 */
class RoundImage
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $commonsId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $thumbnail;

    /**
     * @var boolean
     */
    private $isFinalized;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $starScores;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $binaryScores;

    /**
     * @var \WLM\JuryToolBundle\Entity\Round
     */
    private $round;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->starScores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->binaryScores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commonsId
     *
     * @param integer $commonsId
     * @return RoundImage
     */
    public function setCommonsId($commonsId)
    {
        $this->commonsId = $commonsId;

        return $this;
    }

    /**
     * Get commonsId
     *
     * @return integer 
     */
    public function getCommonsId()
    {
        return $this->commonsId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RoundImage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return RoundImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return RoundImage
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set isFinalized
     *
     * @param boolean $isFinalized
     * @return RoundImage
     */
    public function setIsFinalized($isFinalized)
    {
        $this->isFinalized = $isFinalized;

        return $this;
    }

    /**
     * Get isFinalized
     *
     * @return boolean 
     */
    public function getIsFinalized()
    {
        return $this->isFinalized;
    }

    /**
     * Add starScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageStarScore $starScores
     * @return RoundImage
     */
    public function addStarScore(\WLM\JuryToolBundle\Entity\ImageStarScore $starScores)
    {
        $this->starScores[] = $starScores;

        return $this;
    }

    /**
     * Remove starScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageStarScore $starScores
     */
    public function removeStarScore(\WLM\JuryToolBundle\Entity\ImageStarScore $starScores)
    {
        $this->starScores->removeElement($starScores);
    }

    /**
     * Get starScores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStarScores()
    {
        return $this->starScores;
    }

    /**
     * Add binaryScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores
     * @return RoundImage
     */
    public function addBinaryScore(\WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores)
    {
        $this->binaryScores[] = $binaryScores;

        return $this;
    }

    /**
     * Remove binaryScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores
     */
    public function removeBinaryScore(\WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores)
    {
        $this->binaryScores->removeElement($binaryScores);
    }

    /**
     * Get binaryScores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBinaryScores()
    {
        return $this->binaryScores;
    }

    /**
     * Set round
     *
     * @param \WLM\JuryToolBundle\Entity\Round $round
     * @return RoundImage
     */
    public function setRound(\WLM\JuryToolBundle\Entity\Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return \WLM\JuryToolBundle\Entity\Round 
     */
    public function getRound()
    {
        return $this->round;
    }
    /**
     * @var \DateTime
     */
    private $stamp;


    /**
     * Set stamp
     *
     * @param \DateTime $stamp
     * @return RoundImage
     */
    public function setStamp($stamp)
    {
        $this->stamp = $stamp;

        return $this;
    }

    /**
     * Get stamp
     *
     * @return \DateTime 
     */
    public function getStamp()
    {
        return $this->stamp;
    }
    /**
     * @var \DateTime
     */
    private $timestamp;


    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return RoundImage
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    /**
     * @var \WLM\JuryToolBundle\Entity\RoundImageLock
     */
    private $lock;


    /**
     * Set lock
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImageLock $lock
     * @return RoundImage
     */
    public function setLock(\WLM\JuryToolBundle\Entity\RoundImageLock $lock = null)
    {
        $this->lock = $lock;

        return $this;
    }

    /**
     * Get lock
     *
     * @return \WLM\JuryToolBundle\Entity\RoundImageLock 
     */
    public function getLock()
    {
        return $this->lock;
    }

    public function getScores()
    {
        switch ($this->getRound()->getScoringMode()) {
            case "stars":
                return $this->getStarScores();
            case "binary":
                return $this->getBinaryScores();
            default:
                return null;
        }
    }
}

<?php

namespace WLM\JuryToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageStarScore
 */
class ImageStarScore
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $score;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return ImageStarScore
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }
    /**
     * @var \WLM\JuryToolBundle\Entity\RoundImage
     */
    private $roundImage;


    /**
     * Set roundImage
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImage $roundImage
     * @return ImageStarScore
     */
    public function setRoundImage(\WLM\JuryToolBundle\Entity\RoundImage $roundImage = null)
    {
        $this->roundImage = $roundImage;

        return $this;
    }

    /**
     * Get roundImage
     *
     * @return \WLM\JuryToolBundle\Entity\RoundImage 
     */
    public function getRoundImage()
    {
        return $this->roundImage;
    }
    /**
     * @var \WLM\JuryToolBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \WLM\JuryToolBundle\Entity\User $user
     * @return ImageStarScore
     */
    public function setUser(\WLM\JuryToolBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \WLM\JuryToolBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}

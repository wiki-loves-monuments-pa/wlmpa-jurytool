<?php

namespace WLM\JuryToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;


    public function __construct()
    {
    	parent::__construct();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $starScores;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $binaryScores;


    /**
     * Add starScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageStarScore $starScores
     * @return User
     */
    public function addStarScore(\WLM\JuryToolBundle\Entity\ImageStarScore $starScores)
    {
        $this->starScores[] = $starScores;

        return $this;
    }

    /**
     * Remove starScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageStarScore $starScores
     */
    public function removeStarScore(\WLM\JuryToolBundle\Entity\ImageStarScore $starScores)
    {
        $this->starScores->removeElement($starScores);
    }

    /**
     * Get starScores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStarScores()
    {
        return $this->starScores;
    }

    /**
     * Add binaryScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores
     * @return User
     */
    public function addBinaryScore(\WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores)
    {
        $this->binaryScores[] = $binaryScores;

        return $this;
    }

    /**
     * Remove binaryScores
     *
     * @param \WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores
     */
    public function removeBinaryScore(\WLM\JuryToolBundle\Entity\ImageBinaryScore $binaryScores)
    {
        $this->binaryScores->removeElement($binaryScores);
    }

    /**
     * Get binaryScores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBinaryScores()
    {
        return $this->binaryScores;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rounds;


    /**
     * Add rounds
     *
     * @param \WLM\JuryToolBundle\Entity\Round $rounds
     * @return User
     */
    public function addRound(\WLM\JuryToolBundle\Entity\Round $rounds)
    {
        $this->rounds[] = $rounds;

        return $this;
    }

    /**
     * Remove rounds
     *
     * @param \WLM\JuryToolBundle\Entity\Round $rounds
     */
    public function removeRound(\WLM\JuryToolBundle\Entity\Round $rounds)
    {
        $this->rounds->removeElement($rounds);
    }

    /**
     * Get rounds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRounds()
    {
        return $this->rounds;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $locks;


    /**
     * Add locks
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImageLock $locks
     * @return User
     */
    public function addLock(\WLM\JuryToolBundle\Entity\RoundImageLock $locks)
    {
        $this->locks[] = $locks;

        return $this;
    }

    /**
     * Remove locks
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImageLock $locks
     */
    public function removeLock(\WLM\JuryToolBundle\Entity\RoundImageLock $locks)
    {
        $this->locks->removeElement($locks);
    }

    /**
     * Get locks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocks()
    {
        return $this->locks;
    }
}

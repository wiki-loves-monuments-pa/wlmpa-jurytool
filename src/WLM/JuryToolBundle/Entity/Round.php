<?php

namespace WLM\JuryToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Round
 */
class Round
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $userMode;

    /**
     * @var string
     */
    private $scoringMode;
    
    /**
     * @var string
     */
    private $input;

    /**
     * @var string
     */
    private $scoringPolicy;

    /**
     * @var boolean
     */
    private $isCurrent;
    
    public static $ROUND_USER_MODE_OPEN = "open";
    
    public static $ROUND_USER_MODE_INVITATIONAL = "invitational";
    
    public static $ROUND_SCORING_MODE_BINARY = "binary";
    
    public static $ROUND_SCORING_MODE_STARS = "stars";
    
    public static $ROUND_INPUT_CATEGORY = "category";
    
    public static $ROUND_INPUT_PLAINTEXT = "plaintext";
    
    public static $ROUND_INPUT_PREVIOUS = "previous";
    
    public static $ROUND_SCORING_POLICY_ONE2ONE = "one2one";
    
    public static $ROUND_SCORING_POLICY_ATLEAST = "atleast";

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userMode
     *
     * @param string $userMode
     * @return Round
     */
    public function setUserMode($userMode)
    {
        $this->userMode = $userMode;

        return $this;
    }

    /**
     * Get userMode
     *
     * @return string 
     */
    public function getUserMode()
    {
        return $this->userMode;
    }

    /**
     * Set scoringMode
     *
     * @param string $scoringMode
     * @return Round
     */
    public function setScoringMode($scoringMode)
    {
        $this->scoringMode = $scoringMode;

        return $this;
    }

    /**
     * Get scoringMode
     *
     * @return string 
     */
    public function getScoringMode()
    {
        return $this->scoringMode;
    }

    /**
     * Set isCurrent
     *
     * @param boolean $isCurrent
     * @return Round
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = $isCurrent;

        return $this;
    }

    /**
     * Get isCurrent
     *
     * @return boolean 
     */
    public function getIsCurrent()
    {
        return $this->isCurrent;
    }

    /**
     * Set input
     *
     * @param string $input
     * @return Round
     */
    public function setInput($input)
    {
        $this->input = $input;

        return $this;
    }

    /**
     * Get input
     *
     * @return string 
     */
    public function getInput()
    {
        return $this->input;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add images
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImage $images
     * @return Round
     */
    public function addImage(\WLM\JuryToolBundle\Entity\RoundImage $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImage $images
     */
    public function removeImage(\WLM\JuryToolBundle\Entity\RoundImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }


    /**
     * Set scoringPolicy
     *
     * @param string $scoringPolicy
     * @return Round
     */
    public function setScoringPolicy($scoringPolicy)
    {
        $this->scoringPolicy = $scoringPolicy;

        return $this;
    }

    /**
     * Get scoringPolicy
     *
     * @return string 
     */
    public function getScoringPolicy()
    {
        return $this->scoringPolicy;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;


    /**
     * Add users
     *
     * @param \WLM\JuryToolBundle\Entity\User $users
     * @return Round
     */
    public function addUser(\WLM\JuryToolBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \WLM\JuryToolBundle\Entity\User $users
     */
    public function removeUser(\WLM\JuryToolBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function getScoreClassName()
    {
        switch ($this->scoringMode) {
        	case Round::$ROUND_SCORING_MODE_STARS:
        	    return 'WLMJuryToolBundle:ImageStarScore';
    
        	case Round::$ROUND_SCORING_MODE_BINARY:
        	    return 'WLMJuryToolBundle:ImageBinaryScore';
        }
    }
    /**
     * @var boolean
     */
    private $canViewOtherVotes;


    /**
     * Set canViewOtherVotes
     *
     * @param boolean $canViewOtherVotes
     * @return Round
     */
    public function setCanViewOtherVotes($canViewOtherVotes)
    {
        $this->canViewOtherVotes = $canViewOtherVotes;

        return $this;
    }

    /**
     * Get canViewOtherVotes
     *
     * @return boolean 
     */
    public function getCanViewOtherVotes()
    {
        return $this->canViewOtherVotes;
    }
}

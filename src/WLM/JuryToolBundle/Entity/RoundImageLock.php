<?php

namespace WLM\JuryToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundImageLock
 */
class RoundImageLock
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $timestamp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return RoundImageLock
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    /**
     * @var \WLM\JuryToolBundle\Entity\RoundImage
     */
    private $roundImage;

    /**
     * @var \WLM\JuryToolBundle\Entity\User
     */
    private $user;


    /**
     * Set roundImage
     *
     * @param \WLM\JuryToolBundle\Entity\RoundImage $roundImage
     * @return RoundImageLock
     */
    public function setRoundImage(\WLM\JuryToolBundle\Entity\RoundImage $roundImage = null)
    {
        $this->roundImage = $roundImage;

        return $this;
    }

    /**
     * Get roundImage
     *
     * @return \WLM\JuryToolBundle\Entity\RoundImage 
     */
    public function getRoundImage()
    {
        return $this->roundImage;
    }

    /**
     * Set user
     *
     * @param \WLM\JuryToolBundle\Entity\User $user
     * @return RoundImageLock
     */
    public function setUser(\WLM\JuryToolBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \WLM\JuryToolBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}

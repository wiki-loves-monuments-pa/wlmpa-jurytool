The tool is based on Symfony 2, so all PHP developers are invited to
collaborate. Here are the installation instructions

1. Clone the repository

git clone https://git.gitorious.org/wiki-loves-monuments-pa/wlmpa-jurytool.git

2. Change into the repository and use composer to download symfony

cd wlmpa-jurytool
php route/to/composer.phar update

3. Install the following PHP extensions
 * GD (http://www.php.net/manual/en/book.image.php)
 * Curl (http://www.php.net/manual/en/book.curl.php)

4. Edit app/config/parameters.yml and set the database name, user name
and password

5. Create schema

app/console doctrine:schema:create

6. Install assets (javascript code, images)

app/console assets:install

7. Create a super user

app/console fos:user:create --super-admin admin test at example.com pa$$w0rd

8. Use your browser to navigate to wlmpa-jurytool/web/app_dev.php (the
exact URL depends on your web server configuration)

9. Log in using the administrator credentials created in step 5, and
follow instructions.